#-*- coding: utf-8 -*-
from django.db.models import *
from django.utils.translation import ugettext_lazy as _, ugettext
from datetime import time, datetime
from subprocess import Popen, PIPE

class Zone(Model):
    TYPES = (
                ('master',_('master')),
                ('forward',_('forward')),
                ('hint',_('hint')),
                ('slave',_('slave')),
                ('stub',_('stub')),
                ('delegation-only',_('delegation-only')),
            )
    CHECK_NAMES = (
                (None, _('nothing'))
                ('warn',_('warning'))
                ('fail',_('fail'))
                ('ignore',_('ignore'))
            )
    name = CharField(max_length=80, editable=True, null=False, blank=False, verbose_name=_(u'zone name'))
    type = CharField(max_length=20, editable=True, default='master', null=False, blank=False, verbose_name=_(u'type'), db_index=True, choices=TYPES)
    masters = ForeignKey("Server", blank=True, null=True, verbose_name=_('masters'), db_index=True)
    #file = ForeignKey("", blank=True, null=True, verbose_name=_('masters'), db_index=True)
    check_names = CharField(max_length=10, editable=True, default=None, null=True, blank=False, verbose_name=_(u'check names'), choices=CHECK_NAMES)

    def __unicode__(self, *args, **kwargs):
        return u''


    class Meta:
        ordering = ['-start', 'src', 'disposition']
        unique_together = (('uniqueid',) , ('src', 'dst', 'start',))
        verbose_name = _('call')
        verbose_name_plural = _('calls')

class Server(Model):
    address = IPAddressField(blank=False, null=False, verbose_name=_('server address'), editable=True)
    name = CharField(max_length=50, blank=False, null=False, verbose_name=_('description'), editable=True)

    def __unicode__(self, *args, **kwargs):
        return u'%s (%s)' % (self.address, self.name)

    class Meta:
        ordering = ['address', ]
        verbose_name = _('server')
        verbose_name_plural = _('servers')


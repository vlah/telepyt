#-*- coding: utf-8 -*-
# vim: set foldmarker=def,}}}
from datetime import datetime, timedelta
import calendar
import re

dns_start = datetime(1985,1,1)

class ZoneException(Exception):
    """ Main exception class for zone"""
    pass
class SerialValueError(ZoneException):
    """ Exception class for Serial """
    pass

class Serial:
    def __init__(self, serial=None):
        if serial==None:
            s = int("%s00" % datetime.now().strftime("%Y%m%d"))
        elif isinstance(serial, type(int())) or isinstance(serial, type(str())):
            try:
                s = int(serial)
            except ValueError:
                raise SerialValueError("Serial must be digits")
            if len(str(serial)) != 10:
                raise SerialValueError("Serial must contain 10 digits")

        s = str(s)
        self.__intserial = ((datetime(int(s[0:4]), int(s[4:6]), int(s[6:8]))-dns_start).days*100)+int(s[8:10])

        if int(self.__intserial) <= 0:
            raise SerialValueError("Serial is impossible. DNS has not yet invented.")
    # }}}
    def __get_serial(self):
        s = int(str(self.__intserial)[:-2])
        c = str(self.__intserial)[-2:]
        td= timedelta(days=s)
        return "%8s%02s" % ((dns_start+td).strftime("%Y%m%d"), c)
    # }}}
    def __contains__(self, s):
        return s in self.__str__()
    # }}}
    def __str__(self,):
        return str(self.__get_serial())
    # }}}
    def __int__(self,):
        return self.__intserial
    # }}}
    def __repr__(self,):
        return self.__str__()
    # }}}
    def __len__(self,):
        return self.__int__()
    # }}}
    def __doc__(self,):
        return """  The most important issue is that this value be incremented
                    after any modification to the zone data. For debugging purposes
                    it has shown to be helpful to encode the modification date into
                    the serial number. The value "1999022301" so is an example of the
                    YYYYMMDDnn scheme and must be replaced by proper values for the
                    year (YYYY, four digits), month (MM, two digits), day of month
                    (DD, two digits) and version per day (nn, two digits). The first
                    version of the day should have the value "01". It is important to
                    preserve the order year - month - day. People using this as a
                    debugging aid must, however, not rely on the date information,
                    since experience shows that after initial setup maintainance of
                    this value is often left to the auto-increment feature the software
                    sometimes provides. Other schemes exist - documentation of
                    which is out of the scope of this document."""
    # }}}
    def __lt__(self, b):
        """ self < b """
        if isinstance(b, type(self)):
            return self.__len__() < len(b)
        else:
            raise SerialValueError("You can only compare the 'Serial' and 'Serial'")
    # }}}
    def __le__(self, b):
        """ self <= b """
        if isinstance(b, type(self)):
            return self.__len__() <= len(b)
        else:
            raise SerialValueError("You can only compare the 'Serial' and 'Serial'")
    # }}}
    def __gt__(self, b):
        """ self > b """
        if isinstance(b, type(self)):
            return self.__len__() > len(b)
        else:
            raise SerialValueError("You can only compare the 'Serial' and 'Serial'")
    # }}}
    def __ge__(self, b):
        """ self >= b """
        if isinstance(b, type(self)):
            return self.__len__() >= len(b)
        else:
            raise SerialValueError("You can only compare the 'Serial' and 'Serial'")
    # }}}
    def __eq__(self, b):
        """ self == b """
        if isinstance(b, type(self)):
            return self.__len__() == len(b)
        else:
            raise SerialValueError("You can only compare the 'Serial' and 'Serial'")
    # }}}
    def __ne__(self, b):
        """ self != b """
        return not self.__eq__(b)
    # }}}
    def __iadd__(self, add):
        self.__intserial += add
        return self
    # }}}
    def __add__(self, add):
        self.__intserial += add
        return self.__init__(serial=self.__get_serial())
    # }}}
    def __sub__(self, add):
        self.__intserial -= add
        return self.__init__(serial=self.__get_serial())
    # }}}

#-*- coding: utf-8 -*-
from django.db.models import *
from django.utils.translation import ugettext_lazy as _, ugettext
from datetime import time, datetime
from timezones import TIME_ZONES
from random import randint, choice
from re import compile
from subprocess import Popen, PIPE
import string


class Cdr(Model):
    DISPOSITIONS=(
        ('ANSWERED',_('Answered')),
        ('FAILED', _('Failed')),
        ('NO ANSWER',_('No answer')),
        ('BUSY',_('Busy')),
        ('DOCUMENTATION',_('Documentation')),
        ('BILL',_('Billing')),
        ('IGNORE',_('Ignore')),
    )
    AMAFLAGS=(
        (1,_('No billing')),
        (2,_('Billing')),
        (3,_('Default (Documentation)')),
    )
    clid = CharField(max_length=80, editable=True, default='', null=False, verbose_name=_(u'name CallerID'), db_index=True)
    src = CharField(max_length=80, editable=True, default='', null=False, verbose_name=_(u'number CallerID'), db_index=True)
    dst = CharField(max_length=80, editable=True, default='', null=False, verbose_name=_(u'destination'), db_index=True)
    dcontext = ForeignKey("Contexts", blank=False, null=False, verbose_name=_('context'), db_index=True, db_column='dcontext')
    channel = CharField(max_length=80, editable=True, default='', null=False, verbose_name=_(u'source channel'))
    dstchannel = CharField(max_length=80, editable=True, default='', null=False, verbose_name=_(u'destination channel'))
    lastapp = CharField(max_length=80, editable=True, default='', null=False, verbose_name=_(u'last application'))
    lastdata = CharField(max_length=80, editable=True, default='', null=False, verbose_name=_(u'last data'))
    start = DateTimeField(editable=True, blank=False, auto_now_add=True, auto_now=False, null=False, verbose_name=_(u'start call'), db_index=True)
    answer = DateTimeField(editable=True, blank=False, auto_now_add=True, auto_now=False, null=False, verbose_name=_(u'start voice transfer'), db_index=True)
    end = DateTimeField(editable=True, blank=False, auto_now_add=True, auto_now=False, null=False, verbose_name=_(u'finish call'), db_index=True)
    duration = PositiveIntegerField(editable=True, default=0, null=False, verbose_name=_(u'call duration'), db_index=True)
    billsec = PositiveIntegerField(editable=True, default=0, null=False, verbose_name=_(u'call duration for biling'), db_index=True)
    disposition = CharField(max_length=45, editable=True, default='', null=False, verbose_name=_(u'status'), choices=DISPOSITIONS, db_index=True)
    amaflags = PositiveIntegerField(editable=True, default=0, null=False, verbose_name=_(u'billing flags'), choices=AMAFLAGS, db_index=True)
    accountcode = ForeignKey('address.Unit', blank=True, null=True, verbose_name=_('organization unit'), db_index=True, db_column='accountcode')
    uniqueid = CharField(max_length=128, editable=True, default='', null=False, verbose_name=_(u'channel ID'), db_index=True)
    userfield = CharField(max_length=255, editable=True, default='', null=False, verbose_name=_(u'user definded field'))
    callrecord = FileField(upload_to='media/sounds/records', editable=False, blank=True, null=True, verbose_name=_(u'call record'))

    def __unicode__(self, *args, **kwargs):
        #return _(u'%(start)s | %(src)s --> %(dst)s dur.: %(dur)s sec.' % {'start': self.start, 'src': self.src, 'dst': self.dst, 'dur': self.billsec})
        return (u'%(start)s | %(src)s --> %(dst)s dur.: %(dur)s sec.' % {'start': self.start, 'src': self.src, 'dst': self.dst, 'dur': self.billsec})


    class Meta:
        ordering = ['-start', 'src', 'disposition']
        unique_together = (('uniqueid',) , ('src', 'dst', 'start',))
        verbose_name = _('call')
        verbose_name_plural = _('calls')

class Contexts(Model):
    IN_OUT =(
                (True, _('incoming')),
                (False, _('outgoing')),
            )
    name = CharField(max_length=15, primary_key=True,  blank=False, null=False, verbose_name=_('context'), editable=True)
    full_name = CharField(max_length=15, blank=False, null=False, verbose_name=_('description'), editable=True)
    incoming = BooleanField(default=False, null=False, blank=False, verbose_name=_('incomming'), choices=IN_OUT, db_index=True)

    def __unicode__(self, *args, **kwargs):
        return u'%s' % (self.full_name)

    class Meta:
        ordering = ['name', ]
        verbose_name = _('context')
        verbose_name_plural = _('contexts')

def availAPPS():
        APPS={
                "AddQueueMember": _("AddQueueMember: Dynamically adds queue members"),
                "ADSIProg": _("ADSIProg: Load Asterisk ADSI Scripts into phone"),
                "AELSub": _("AELSub: Launch subroutine built with AEL"),
                "AgentLogin": _("AgentLogin: Call agent login"),
                "AgentMonitorOutgoing": _("AgentMonitorOutgoing: Record agent's outgoing call"),
                "AGI": _("AGI: Executes an AGI compliant application"),
                "AlarmReceiver": _("AlarmReceiver: Provide support for receiving alarm reports from a burglar or fire alarm panel"),
                "AMD": _("AMD: Attempt to detect answering machines"),
                "Answer": _("Answer: Answer a channel if ringing"),
                "Authenticate": _("Authenticate: Authenticate a user"),
                "BackGround": _("BackGround: Play an audio file while waiting for digits of an extension to go to"),
                "BackgroundDetect": _("BackgroundDetect: Background a file with talk detect"),
                "Bridge": _("Bridge: Bridge two channels"),
                "Busy": _("Busy: Indicate the Busy condition"),
                "CallCompletionCancel": _("CallCompletionCancel: Cancel call completion service"),
                "CallCompletionRequest": _("CallCompletionRequest: Request call completion service for previous call"),
                "CELGenUserEvent": _("CELGenUserEvent: Generates a CEL User Defined Event"),
                "ChangeMonitor": _("ChangeMonitor: Change monitoring filename of a channel"),
                "ChanIsAvail": _("ChanIsAvail: Check channel availability"),
                "ChannelRedirect": _("ChannelRedirect: Redirects given channel to a dialplan target"),
                "ChanSpy": _("ChanSpy: Listen to a channel, and optionally whisper into it"),
                "ClearHash": _("ClearHash: Clear the keys from a specified hashname"),
                "ConfBridge": _("ConfBridge: Conference bridge application"),
                "Congestion": _("Congestion: Indicate the Congestion condition"),
                "ContinueWhile": _("ContinueWhile: Restart a While loop"),
                "ControlPlayback": _("ControlPlayback: Play a file with fast forward and rewind"),
                "DAHDIAcceptR2Call": _("DAHDIAcceptR2Call: Accept an R2 call if its not already accepted (you still need to answer it)"),
                "DAHDIBarge": _("DAHDIBarge: Barge in (monitor) DAHDI channel"),
                "DAHDIRAS": _("DAHDIRAS: Executes DAHDI ISDN RAS application"),
                "DAHDIScan": _("DAHDIScan: Scan DAHDI channels to monitor calls"),
                "DAHDISendCallreroutingFacility": _("DAHDISendCallreroutingFacility: Send an ISDN call rerouting/deflection facility message"),
                "DAHDISendKeypadFacility": _("DAHDISendKeypadFacility: Send digits out of band over a PRI"),
                "DateTime": _("DateTime: Says a specified time in a custom format"),
                "DBdel": _("DBdel: Delete a key from the asterisk database"),
                "DBdeltree": _("DBdeltree: Delete a family or keytree from the asterisk database"),
                "DeadAGI": _("DeadAGI: Executes AGI on a hungup channel"),
                "Dial": _("Dial: Attempt to connect to another device or endpoint and bridge the call"),
                "Dictate": _("Dictate: Virtual Dictation Machine"),
                "Directory": _("Directory: Provide directory of voicemail extensions"),
                "DISA": _("DISA: Direct Inward System Access"),
                "DumpChan": _("DumpChan: Dump Info About The Calling Channel"),
                "EAGI": _("EAGI: Executes an EAGI compliant application"),
                "Echo": _("Echo: Echo audio, video, DTMF back to the calling party"),
                "EndWhile": _("EndWhile: End a while loop"),
                "Exec": _("Exec: Executes dialplan application"),
                "ExecIf": _("ExecIf: Executes dialplan application, conditionally"),
                "ExecIfTime": _("ExecIfTime: Conditional application execution based on the current time"),
                "ExitWhile": _("ExitWhile: End a While loop"),
                "ExtenSpy": _("ExtenSpy: Listen to a channel, and optionally whisper into it"),
                "ExternalIVR": _("ExternalIVR: Interfaces with an external IVR application"),
                "Festival": _("Festival: Say text to the user"),
                "Flash": _("Flash: Flashes a DAHDI Trunk"),
                "FollowMe": _("FollowMe: Find-Me/Follow-Me application"),
                "ForkCDR": _("ForkCDR: Forks the Call Data Record"),
                "GetCPEID": _("GetCPEID: Get ADSI CPE ID"),
                "Gosub": _("Gosub: Jump to label, saving return address"),
                "GosubIf": _("GosubIf: Conditionally jump to label, saving return address"),
                "Goto": _("Goto: Jump to a particular priority, extension, or context"),
                "GotoIf": _("GotoIf: Conditional goto"),
                "GotoIfTime": _("GotoIfTime: Conditional Goto based on the current time"),
                "Hangup": _("Hangup: Hang up the calling channel"),
                "IAX2Provision": _("IAX2Provision: Provision a calling IAXy with a given template"),
                "ICES": _("ICES: Encode and stream using 'ices'"),
                "ImportVar": _("ImportVar: Import a variable from a channel into a new variable"),
                "Incomplete": _("Incomplete: Returns AST_PBX_INCOMPLETE value"),
                "JabberJoin": _("JabberJoin: Join a chat room"),
                "JabberLeave": _("JabberLeave: Leave a chat room"),
                "JabberSend": _("JabberSend: Sends an XMPP message to a buddy"),
                "JabberSendGroup": _("JabberSendGroup: Send a Jabber Message to a specified chat room"),
                "JabberStatus": _("JabberStatus: Retrieve the status of a jabber list member"),
                "JACK": _("JACK: Jack Audio Connection Kit"),
                "Log": _("Log: Send arbitrary text to a selected log level"),
                "Macro": _("Macro: Macro Implementation"),
                "MacroExclusive": _("MacroExclusive: Exclusive Macro Implementation"),
                "MacroExit": _("MacroExit: Exit from Macro"),
                "MacroIf": _("MacroIf: Conditional Macro implementation"),
                "MailboxExists": _("MailboxExists: Check to see if Voicemail mailbox exists"),
                "MeetMe": _("MeetMe: MeetMe conference bridge."),
                "MeetMeAdmin": _("MeetMeAdmin: MeetMe conference administration"),
                "MeetMeChannelAdmin": _("MeetMeChannelAdmin: MeetMe conference Administration"),
                "MeetMeCount": _("MeetMeCount: MeetMe participant count"),
                "Milliwatt": _("Milliwatt: Generate a Constant 1004Hz tone at 0dbm (mu-law)"),
                "MinivmAccMess": _("MinivmAccMess: Record account specific messages"),
                "MinivmDelete": _("MinivmDelete: Delete Mini-Voicemail voicemail messages"),
                "MinivmGreet": _("MinivmGreet: Play Mini-Voicemail prompts"),
                "MinivmMWI": _("MinivmMWI: Send Message Waiting Notification to subscriber(s) of mailbox"),
                "MinivmNotify": _("MinivmNotify: Notify voicemail owner about new messages"),
                "MinivmRecord": _("MinivmRecord: Receive Mini-Voicemail and forward via e-mail"),
                "MixMonitor": _("MixMonitor: Record a call and mix the audio during the recording"),
                "Monitor": _("Monitor: Monitor a channel"),
                "Morsecode": _("Morsecode: Plays morse code"),
                "MP3Player": _("MP3Player: Play an MP3 file or M3U playlist file or stream"),
                "MSet": _("MSet: Set channel variable(s) or function value(s)"),
                "MusicOnHold": _("MusicOnHold: Play Music On Hold indefinitely"),
                "MYSQL": _("MYSQL: Do several mySQLy things"),
                "NBScat": _("NBScat: Play an NBS local stream"),
                "NoCDR": _("NoCDR: Tell Asterisk to not maintain a CDR for the current call"),
                "NoOp": _("NoOp: Do Nothing (No Operation)"),
                "ODBC_Commit": _("ODBC_Commit: Commits a currently open database transaction"),
                "ODBC_Rollback": _("ODBC_Rollback: Rollback a currently open database transaction"),
                "ODBCFinish": _("ODBCFinish: Clear the resultset of a sucessful multirow query"),
                "Originate": _("Originate: Originate a call"),
                "Page": _("Page: Page series of phones"),
                "Park": _("Park: Park yourself"),
                "ParkAndAnnounce": _("ParkAndAnnounce: Park and Announce"),
                "ParkedCall": _("ParkedCall: Retrieve a parked call"),
                "PauseMonitor": _("PauseMonitor: Pause monitoring of a channel"),
                "PauseQueueMember": _("PauseQueueMember: Pauses a queue member"),
                "Pickup": _("Pickup: Directed extension call pickup"),
                "PickupChan": _("PickupChan: Pickup a ringing channel"),
                "Playback": _("Playback: Play a file"),
                "PlayTones": _("PlayTones: Play a tone list"),
                "PrivacyManager": _("PrivacyManager: Require phone number to be entered, if no CallerID sent"),
                "Proceeding": _("Proceeding: Indicate proceeding"),
                "Progress": _("Progress: Indicate progress"),
                "Queue": _("Queue: Queue a call for a call queue"),
                "QueueLog": _("QueueLog: Writes to the queue_log file"),
                "RaiseException": _("RaiseException: Handle an exceptional condition"),
                "Read": _("Read: Read a variable"),
                "ReadExten": _("ReadExten: Read an extension into a variable"),
                "ReadFile": _("ReadFile: Read the contents of a text file into a channel variable"),
                "ReceiveFAX": _("ReceiveFAX: Receive a Fax"),
                "Record": _("Record: Record to a file"),
                "RemoveQueueMember": _("RemoveQueueMember: Dynamically removes queue members"),
                "ResetCDR": _("ResetCDR: Resets the Call Data Record"),
                "RetryDial": _("RetryDial: Place a call, retrying on failure allowing an optional exit extension"),
                "Return": _("Return: Return from gosub routine"),
                "Ringing": _("Ringing: Indicate ringing tone"),
                "SayAlpha": _("SayAlpha: Say Alpha"),
                "SayCountPL": _("SayCountPL: Say Polish counting words"),
                "SayDigits": _("SayDigits: Say Digits"),
                "SayNumber": _("SayNumber: Say Number"),
                "SayPhonetic": _("SayPhonetic: Say Phonetic"),
                "SayUnixTime": _("SayUnixTime: Says a specified time in a custom format"),
                "SendDTMF": _("SendDTMF: Sends arbitrary DTMF digits"),
                "SendFAX": _("SendFAX: Send a Fax"),
                "SendImage": _("SendImage: Sends an image file"),
                "SendText": _("SendText: Send a Text Message"),
                "SendURL": _("SendURL: Send a URL"),
                "Set": _("Set: Set channel variable or function value"),
                "SetAMAFlags": _("SetAMAFlags: Set the AMA Flags"),
                "SetCallerPres": _("SetCallerPres: Set CallerID Presentation"),
                "SetMusicOnHold": _("SetMusicOnHold: Set default Music On Hold class"),
                "SIPAddHeader": _("SIPAddHeader: Add a SIP header to the outbound call"),
                "SIPDtmfMode": _("SIPDtmfMode: Change the dtmfmode for a SIP call"),
                "SIPRemoveHeader": _("SIPRemoveHeader: Remove SIP headers previously added with SIPAddHeader"),
                "SLAStation": _("SLAStation: Shared Line Appearance Station"),
                "SLATrunk": _("SLATrunk: Shared Line Appearance Trunk"),
                "SMS": _("SMS: Communicates with SMS service centres and SMS capable analogue phones"),
                "SoftHangup": _("SoftHangup: Hangs up the requested channel"),
                "SpeechActivateGrammar": _("SpeechActivateGrammar: Activate a grammar"),
                "SpeechBackground": _("SpeechBackground: Play a sound file and wait for speech to be recognized"),
                "SpeechCreate": _("SpeechCreate: Create a Speech Structure"),
                "SpeechDeactivateGrammar": _("SpeechDeactivateGrammar: Deactivate a grammar"),
                "SpeechDestroy": _("SpeechDestroy: End speech recognition"),
                "SpeechLoadGrammar": _("SpeechLoadGrammar: Load a grammar"),
                "SpeechProcessingSound": _("SpeechProcessingSound: Change background processing sound"),
                "SpeechStart": _("SpeechStart: Start recognizing voice in the audio stream"),
                "SpeechUnloadGrammar": _("SpeechUnloadGrammar: Unload a grammar"),
                "StackPop": _("StackPop: Remove one address from gosub stack"),
                "StartMusicOnHold": _("StartMusicOnHold: Play Music On Hold"),
                "StopMixMonitor": _("StopMixMonitor: Stop recording a call through MixMonitor, and free the recording's file handle"),
                "StopMonitor": _("StopMonitor: Stop monitoring a channel"),
                "StopMusicOnHold": _("StopMusicOnHold: Stop playing Music On Hold"),
                "StopPlayTones": _("StopPlayTones: Stop playing a tone list"),
                "System": _("System: Execute a system command"),
                "TestClient": _("TestClient: Execute Interface Test Client"),
                "TestServer": _("TestServer: Execute Interface Test Server"),
                "Transfer": _("Transfer: Transfer caller to remote extension"),
                "TryExec": _("TryExec: Executes dialplan application, always returning"),
                "TrySystem": _("TrySystem: Try executing a system command"),
                "UnpauseMonitor": _("UnpauseMonitor: Unpause monitoring of a channel"),
                "UnpauseQueueMember": _("UnpauseQueueMember: Unpauses a queue member.      "),
                "UserEvent": _("UserEvent: Send an arbitrary event to the manager interface"),
                "Verbose": _("Verbose: Send arbitrary text to verbose output"),
                "VMAuthenticate": _("VMAuthenticate: Authenticate with Voicemail passwords"),
                "VMSayName": _("VMSayName: Play the name of a voicemail user"),
                "VoiceMail": _("VoiceMail: Leave a Voicemail message"),
                "VoiceMailMain": _("VoiceMailMain: Check Voicemail messages"),
                "Wait": _("Wait: Waits for some time"),
                "WaitExten": _("WaitExten: Waits for an extension to be entered"),
                "WaitForNoise": _("WaitForNoise: Waits for a specified amount of noise"),
                "WaitForRing": _("WaitForRing: Wait for Ring Application"),
                "WaitForSilence": _("WaitForSilence: Waits for a specified amount of silence"),
                "WaitMusicOnHold": _("WaitMusicOnHold: Wait, playing Music On Hold"),
                "WaitUntil": _("WaitUntil: Wait (sleep) until the current time is the given epoch"),
                "While": _("While: Start a while loop"),
                "Zapateller": _("Zapateller: Block telemarketers with SIT"),
        }

        apps = Popen("""/usr/sbin/asterisk -rx "core show applications" """, stdout=PIPE, stderr=PIPE, shell=True)
        re = compile("\s{0,}(\S+):[\S\s]+")
        stdout = apps.stdout.readlines()

        if len(stdout)<2:
            return APPS.items()

        apps_small=dict([(i.lower(), i) for i in APPS.keys()])
        aster_apps = list()

        for app in stdout:
            try:
                aster_apps.append(re.findall(app)[0].lower().replace(" ", ""))
            except IndexError:
                pass

        real_apps = list(set(aster_apps) & set(apps_small.keys()))
        unknown_a = list(set(aster_apps) - set(apps_small.keys()))

        real_apps.sort()
        unknown_a.sort()

        out = list()

        for i in unknown_a:
            out.append((i, _("%(appname)s: Unknown application" % {'appname': i})))

        for i in real_apps:
            out.append((apps_small[i],APPS[apps_small[i]]))

        return out

APPS=availAPPS()

class Extensions(Model):
    COMMENTED=(
        (1,_('No')),
        (0,_('Yes'))
    )

    commented = SmallIntegerField(default=0, null=False, blank=False, verbose_name='включен', choices=COMMENTED, db_index=True)
    context = ForeignKey(Contexts, blank=False, null=False, verbose_name='контекст', db_index=True, db_column='context')
    exten = CharField(max_length=80, blank=False, null=False, verbose_name='экстен/шаблон', db_index=True, db_column='exten')
    priority = IntegerField(null=False, blank=False, verbose_name='приоритет',)
    app = CharField(max_length=80, blank=False, null=False, verbose_name='приложение диалплана', db_index=True, db_column='app', choices=APPS)
    appdata = CharField(max_length=200, blank=True, null=True, verbose_name='параметры приложения', editable=True, db_index=True)

    class Meta:
        ordering = ['context__name', 'exten', 'priority']
        unique_together = (('context', 'exten', 'priority',) ,)
        verbose_name = 'экстеншн'
        verbose_name_plural = 'экстеншны'

    def __unicode__(self, *args, **kwargs):
        return u"%s| exten => %s,%s,%s(%s)" % (self.context, self.exten, self.priority, self.app, self.appdata)

class Numbers(Model):
    YESNO=(
        ('yes',_('Allow')),
        ('no', _('Deny')),
    )
    TRUEFALSE=(
        (False,'Нет'),
        (True,'Да')
    )
    COMMENTED=(
        (1,'Нет'),
        (0,'Да')
    )
    ENCRYPTION=(
        ('yes',_(u'Yes (Enabling SRTP)')),
        ('no',_(u'No (Unsecure usage)')),
        ('aes_80',_(u'AES80 (Working on Linksys)')),
        ('aes_32',_(u'AES32')),
        ('f8_80',_(u'F8 80')),
    )
    TRANSPORT=(
        ('udp','UDP'),
        ('tcp','TCP'),
        ('tls','TLS'),
    )
    TYPES=(
        ('peer','Только исходящие (peer)'),
        ('user','Только входящие (user)'),
        ('friend','Входящие и исходящие (friend)')
    )
    AMAFLAGS=(
        ('omit','Не обсчитывать (1)'),
        ('billing','Обсчитывать (2)'),
        ('default','По умолчанию (3)'),
        ('documentation','Документировать (3)'),
    )
    DTMFS=(
        ('inband','В потоке звука'),
        ('rfc2833','RFC2833'),
        ('info','SIP info DTMF'),
        ('auto','Автоопределение'),
    )
    INVITE=(
        ('port', "Игнорировать номер порта, с которого пришла аутентификация"),
        ('invite', "Не требовать начальное сообщение INVITE для аутентификации"),
        ("port,invite", """Не требовать начальное сообщение INVITE для аутентификации и игнорировать порт, с которого пришел запрос"""),
    )
    commented = SmallIntegerField(default=0, verbose_name='включен', choices=COMMENTED, db_index=True)
    name = CharField(max_length=15, verbose_name='номер', primary_key=True)
    host = CharField(max_length=25, verbose_name='хост', default='dynamic', help_text='привязка к определенному хосту или IP, или \'dynamic\'')
    nat = CharField(max_length=5, default='no', editable=True, verbose_name='NAT', help_text='разрешать ли работу через NAT', choices=YESNO)
    type = CharField(max_length=8, default='friend', editable=True, verbose_name=u'тип', help_text='тип пользователя', choices=TYPES)
    accountcode = ForeignKey('address.Unit', blank=True, null=True, verbose_name=u'принадлежит', db_column='accountcode')
    amaflags = CharField(max_length=20, default='billing', blank=False, null=False, editable=True, verbose_name='флаги биллинга', help_text='специальные флаги для управления обсчетом по умолчанию', choices=AMAFLAGS)
    callgroup = CharField(max_length=25, blank=True, null=True, editable=False, help_text='не используется, для совместимости')
    callerid = CharField(max_length=250, blank=True, null=True, editable=True, help_text='Оставить пустым для автоподстановки')
    cancallforward = CharField(max_length=3, default='yes', blank=False, null=False, editable=True, verbose_name='Перевод звонков', help_text='разрешать ли перевод звонков', choices=YESNO)
    directmedia = CharField(max_length=3, default='no', editable=True, verbose_name='Прямой поток', help_text='разрешать ли прямое прохождение трафика', choices=YESNO)
    context = ForeignKey(Contexts, blank=True, null=True, verbose_name='контекст', db_column='context')
    defaultip = CharField(max_length=25, blank=True, null=True, verbose_name='IP клиента', help_text='Если Вы знаете IP адрес телефона, Вы можете указать его здесь. Эти настройки будут использоваться при совершении вызовов на данный телефон, если он еще не зарегистрировался на сервере. После регистрации, телефон сам сообщит Asterisk под каким именем пользователя и IP адресом он доступен.')
    dtmfmode = CharField(max_length=8, default='info', editable=True, verbose_name=u'тип DTMF сигнализации', help_text='в режиме auto Asterisk будет использовать режим rfc2833 для передачи DTMF, по умолчанию, но будет переключаться в режим inband, для передачи DTMF сигналов, если удаленный клиент не укажет в SDP сообщении, что он поддерживает режим передачи DTMF - rfc2833', choices=DTMFS)
    fromuser = CharField(max_length=80, blank=True, null=True, editable=False, help_text='не используется, для совместимости')
    fromdomain = CharField(max_length=80, blank=True, null=True, editable=False, help_text='не используется, для совместимости')
    insecure = CharField(max_length=20, default='', blank=True, null=True, editable=True, verbose_name='игнорировать', choices=INVITE)
    language = CharField(max_length=2, editable=True, default='ru', verbose_name='язык')
    mailbox = CharField(max_length=15, blank=False, null=True, editable=False, help_text='Оставить пустым для автоподстановки')
    md5secret = CharField(max_length=80, blank=True, null=True, editable=False, verbose_name='MD5 пароль', help_text='не используется, для совместимости')
    deny = CharField(max_length=25, blank=True, null=True, editable=False, verbose_name='запрещенные подсети')
    permit = CharField(max_length=25, blank=True, null=True, editable=False, verbose_name='разрешенные подсети')
    mask = CharField(max_length=25, blank=True, null=True, editable=False, help_text='устарел')
    musiconhold = ForeignKey('asterfiles.SndFile', db_column='musiconhold', blank=True, null=True, editable=True, verbose_name='музыка ожидания', db_index=True, related_name='musiconhold')
    pickupgroup = CharField(max_length=80, blank=True, null=True, editable=False, help_text='не используется, для совместимости')
    qualify = CharField(max_length=5, default='yes', blank=False, null=False, editable=True, verbose_name='SIP тест', help_text='если yes тогда Asterisk периодически (раз в 2 секунды) будет отправлять SIP сообщение типа OPTIONS, для проверки, что данное устройство работает и доступно для совершения вызовов. Если данное устройство, не ответит в течении заданного периода, тогда Asterisk рассматривает это устройство как выключенное и недоступное для совершения вызовов.', choices=YESNO)
    regexten = CharField(max_length=80, blank=True, null=True, editable=False, help_text='не используется, для совместимости')
    restrictcid = CharField(max_length=25, blank=True, null=True, editable=False, help_text='устарел')
    rtptimeout = CharField(max_length=3, blank=True, null=True, editable=False, help_text='не используется, для совместимости')
    rtpholdtimeout = CharField(max_length=3, blank=True, null=True, editable=False, help_text='не используется, для совместимости')
    secret = CharField(max_length=15, blank=True, null=False, editable=True, verbose_name='пароль', help_text='Для генерации оставьте пустым')
    setvar = CharField(max_length=25, blank=True, null=True, editable=False, help_text='устарел')
    disallow = CharField(max_length=100, editable=True, default='all', verbose_name='запрещенные кодеки')
    allow = CharField(max_length=100, editable=True, default='alaw', verbose_name='разрешенные кодеки')
    comment = TextField(blank=True, null=True, verbose_name='комментарий', max_length=255)
    trustrpid = CharField(max_length=3, blank=True, null=True, editable=True, default='no', verbose_name='Принимать RPID', choices=YESNO, help_text='Можно ли доверять полученному от SIP клиента Remote-Party-ID')
    sendrpid = CharField(max_length=3, blank=True, null=True, editable=True, default='yes', verbose_name='Передавать RPID', choices=YESNO, help_text='Необходимо передавать SIP клиенту Remote-Party-ID')
    videosupport = CharField(max_length=3, blank=True, null=True, editable=True, default='no', choices=YESNO, verbose_name='Поддержка видео')
    fullcontact = CharField(max_length=80, blank=True, null=True, editable=False, help_text='для совместимости')
    ipaddr = IPAddressField(blank=True, null=True, editable=True, verbose_name='последний IP', help_text='для совместимости')
    port = PositiveIntegerField(blank=True, null=True, editable=True, help_text='порт не dynamic клиентов')
    regseconds = BigIntegerField(blank=True, null=True, editable=True, help_text='для совместимости')
    regserver = CharField(max_length=100, blank=True, null=True, editable=True, help_text='для совместимости')
    useragent = CharField(max_length=100, blank=True, null=True, editable=True, help_text='для совместимости')
    defaultuser = CharField(max_length=100, blank=True, null=True, editable=True, help_text='для совместимости')
    useragent = CharField(max_length=100, blank=True, null=True, editable=False, help_text='для совместимости')
    lastms = CharField(max_length=100, blank=True, null=True, editable=False, help_text='для совместимости')
    defaultuser = CharField(max_length=15, blank=True, null=True, editable=False, help_text='сервер Asterisk будет посылать сообщения INVITE на username@defaultip')
    encryption = CharField(max_length=8, blank=False, null=False, editable=True, verbose_name=_(u'encryption'), choices=ENCRYPTION, default='no')
    transport = CharField(max_length=8, blank=False, null=False, editable=True, verbose_name=_(u'transport'), choices=TRANSPORT, default='udp')


    def gen_passwd(self):
        self.secret = ''.join(choice(string.letters.lower()+string.digits) for i in xrange(12))
        if not (self.name == None or self.name == ''):
            self.save()

    def __unicode__(self):
        return u'%s (%s)' % (self.name, self.accountcode)

    def __init__(self,*args,**kwargs):
        super(Numbers,self).__init__(*args, **kwargs)
        if not self.secret.__len__():
            self.gen_passwd()

    def save(self, *args, **kwargs):
        re = compile("^\"(?P<name>[\s\S]+)\" \<(?P<number>[\S\s]+)\>$")

        try:
            t = re.match(self.callerid).groupdict()
        except AttributeError:
            if len(self.callerid) == 0 or self.callerid == None:
                t = {'name': self.name, 'number': self.name}
            else:
                t = {'name': self.callerid.replace('"','').replace("<",'').replace(">",''), 'number': self.name}
        self.callerid = '"%(name)s" <%(number)s>' % t
        return super(Numbers, self).save(*args, **kwargs)

    class Meta:
        unique_together = (('name',),)
        ordering = ['name']
        verbose_name = 'номер'
        verbose_name_plural = 'номера'

class Voicemail(Model):
    YESNO=(
        ('yes','Да'),
        ('no', 'Нет'),
    )

    uniqueid = AutoField(primary_key=True,  blank=False, null=False, verbose_name='id', editable=False)
    payer = ForeignKey('address.Unit', blank=False, null=False, verbose_name='плательщик', db_index=True, db_column='customer_id', editable=False)
    context = CharField(max_length=10, blank=False, null=False, verbose_name='контекст', db_column='context', default=u'city', editable=False, db_index=True)
    mailbox = ForeignKey(Numbers, blank=False, null=False, verbose_name=u'Номер ящика', db_index=True, db_column='mailbox')
    password = PositiveSmallIntegerField(editable=False, blank=True, null=True, verbose_name=u'пароль', db_index=True)
    fullname = TextField(blank=True, null=True, verbose_name='полное имя', max_length=255)
    email = EmailField(blank=True, null=True, verbose_name='электронная почта', db_index=True)
    pager = EmailField(blank=True, null=True, verbose_name='E-mail для пейджера', db_index=True)
    tz = CharField(max_length=20, blank=False, null=False, verbose_name=u'часовой пояс', default=u'Europe/Moscow', choices=TIME_ZONES)
    attach = CharField(max_length=3, blank=False, null=False, verbose_name=u'прикреплять файлы', choices=YESNO, default='yes')
    saycid = CharField(max_length=3, blank=False, null=False, verbose_name=u'указывать ли номер', choices=YESNO, default='yes')
    review = CharField(max_length=3, blank=False, null=False, verbose_name=u'пересмотр', help_text='Если "Да", то дает возможность перезаписывать оставленное сообщение перед отправкой', choices=YESNO, default='no')
    operator = CharField(max_length=3, blank=False, editable=False, null=False, verbose_name=u'вызывать экстеншн 0', help_text='''Если "Да", то если вызывающий абонент нажмет 0 (ноль) в процессе проигрыша анонса, то выполнения плана набора продолжиться с екстеншена 'o' (сокр. от "Out"), в текущем контексте для голосовой почты. Эта функция может использоваться для перехода к вызову секретаря.''', choices=YESNO, default='no')
    envelope = CharField(max_length=3, blank=False, null=False, verbose_name=u'Региональные параметры', choices=YESNO, default='no', help_text='''Применять ли к сообщениям региональные параметры''')
    sayduration = CharField(max_length=3, blank=False, null=False, verbose_name=u'воспроизводить длительность', help_text='''Если "Да", то воспроизводит длительность сообщения''', choices=YESNO, default='no')
    saydurationm = PositiveSmallIntegerField(blank=True, null=False, verbose_name=u'начальная длительность воспроизведения', default=1, help_text='''Если "Воспроизводить длительность" "Да", то воспроизводит длительность сообщения если сообщение более %значение% минут''', db_index=True)
    sendvoicemail = CharField(max_length=3, blank=False, null=False, verbose_name=u'может пересылать сообщения', choices=YESNO, default='no')
    delete = CharField(max_length=3, blank=False, null=False, verbose_name=u'удалять сообщения после отправки', choices=YESNO, default='no', db_index=True)
    nextaftercmd = CharField(max_length=3, blank=False, null=False, verbose_name=u'проигрывать сдедующее после комманды', choices=YESNO, default='yes', db_index=True)
    forcename = CharField(max_length=3, blank=False, null=False, verbose_name=u'принудительно требовать ввод номера ящика', choices=YESNO, default='no')
    forcegreetings = CharField(max_length=3, blank=False, null=False, verbose_name=u'принудительно требовать запись приветствия', choices=YESNO, default='no')
    hidefromdir = CharField(max_length=3, blank=False, null=False, verbose_name=u'X3 hidefromdir', choices=YESNO, default='yes')
    stamp = DateTimeField(verbose_name=u'хз что это', blank=True, null=True, editable=False, db_index=True)
    attachfmt = CharField(max_length=80, default='wav49', blank=False, null=False, editable=False, verbose_name=u'Формат вложений')
    searchcontexts = CharField(max_length=3, blank=True, null=True, verbose_name=u'Пока не ясно но что-то там искать во всех контекстах', choices=YESNO, default='no', db_index=True)
    cidinternalcontexts = CharField(max_length=10, default='', blank=True, null=True, verbose_name="Пока не ясно cidinternalcontexts", db_index=True)
    exitcontext = CharField(max_length=10, default='', blank=True, null=True, verbose_name="Пока не ясно exitcontext", db_index=True)
    volgain = CharField(max_length=3, blank=False, null=False, verbose_name=u'сделать громче на (dB)', db_index=True, default='0.0')
    tempgreetwarn = CharField(max_length=3, blank=False, null=False, verbose_name=u'напоминать что установлено временное приветствие', choices=YESNO, default='yes', db_index=True)
    messagewrap = CharField(max_length=3, blank=False, null=False, verbose_name=u'воспроизводить более позние первыми', choices=YESNO, default='no', db_index=True)
    minpassword = PositiveSmallIntegerField(blank=False, null=False, verbose_name=u'минимальная длинна пароля', default=4, db_index=True)
    listen_control_forward_key = CharField(max_length=2, blank=False, null=False, verbose_name=u'клавиша перемотки вперед', default='6', db_column='listen-control-forward-key')
    listen_control_reverse_key = CharField(max_length=2, blank=False, null=False, verbose_name=u'клавиша перемотки назад', default='4', db_column='listen-control-reverse-key')
    listen_control_pause_key = CharField(max_length=2, blank=False, null=False, verbose_name=u'клавиша перемотки вперед', default='5', db_column='listen-control-pause-key')
    listen_control_restart_key = CharField(max_length=2, blank=False, null=False, verbose_name=u'клавиша повтора', default='2', db_column='listen-control-restart-key')
    listen_control_stop_key = CharField(max_length=2, blank=False, null=False, verbose_name=u'клавиша перемотки вперед', default='8', db_column='listen-control-stop-key')
    backupdeleted = CharField(max_length=2, default='25', blank=False, null=False, verbose_name=u'сообщений в корзине не более', db_index=True)

    class Meta:
         verbose_name = 'голосовая почта'
         verbose_name_plural = 'учетные записи голосовой почты'

    def __unicode__(self):
            return u'%s' % (self.mailbox)

    def gen_passwd(self):
        self.password = randint(1000,9999)

class Queue(Model):
    YESNO=(
        ('yes','Да'),
        ('no', 'Нет'),
    )
    YESNOONCE=(
        ('yes', u'Да'),
        ('no',  u'Нет'),
        ('once', u'Один раз')
    )
    TRUEFALSE=(
        (False,'Нет'),
        (True,'Да'),
    )
    STRATEGY=(
        ('ringall', u'Звонить во все доступные каналы пока не ответят'),
        ('leastrecent', u'Звонить в интерфейс который отвечал последним'),
        ('fewestcalls', u'Звонить тому кто принял меньше всего звонков в этой очереди'),
        ('random', u'Звонить совершенно случайно'),
        ('rrmemory', u'Round-Robin с памятью, Звонить по очереди помня кто последний отвечал'),
        ('wrandom', u'Звонить случайно но использовать веса'),
        ('linear', u'Звонить по порядку перечисленному в самой очереди'),
    )
    MONITOR_FORMAT=(
        ('gsm', u'GSM'),
        ('wav', u'WAV'),
        ('wav49', u'WAV, в котором записан GSM 6.10 кодек в MS формате'),
    )
    name = CharField(db_column='name', max_length=128, blank=False, null=False, verbose_name=u'имя', editable=True, db_index=True, primary_key=True)
    musiconhold = CharField(max_length=128, blank=True, null=True, editable=True, db_index=True)
    announce = CharField(max_length=128, blank=True, null=True, editable=True, db_index=True, verbose_name=u'Анонс', help_text=u'использовать анонс, проиграть этот файл анонса из файла',)
    context = ForeignKey("Contexts", blank=True, null=True, verbose_name='контекст', db_index=True, db_column='context', help_text=u'контекст в который будут перенаправлен ожидающий вызов набравший номер')
    timeout = PositiveIntegerField(blank=True, null=True, editable=True, db_index=True, help_text=u'столько секунд звонок без ответа на трубке прежде чем он перейдет на следующего агента')
    monitor_join = BooleanField(default=False, null=False, blank=False, verbose_name=u'Смешивать запись', choices=TRUEFALSE, db_index=True)
    monitor_format = CharField(max_length=128, blank=True, null=True, editable=True, db_index=True, choices=MONITOR_FORMAT)
    queue_youarenext = ForeignKey('asterfiles.SndFile', db_column='queue_youarenext', blank=True, null=True, editable=True, db_index=True, help_text=u'сообщение, которое будет сыграно: Теперь Вы первый на линии.', related_name='queue_youarenext')
    queue_thereare = ForeignKey('asterfiles.SndFile', db_column='queue_thereare', blank=True, null=True, editable=True, db_index=True, help_text=u'сообщение, которое будет сыграно: Ваша позиция в очереди', related_name='queue_thereare')
    queue_callswaiting = ForeignKey('asterfiles.SndFile', db_column='queue_callswaiting', blank=True, null=True, editable=True, db_index=True, help_text=u'сообщение, которое будет сыграно: ожидайте ответа', related_name='queue_callswaiting')
    queue_holdtime = ForeignKey('asterfiles.SndFile', db_column='queue_holdtime', blank=True, null=True, editable=True, db_index=True, help_text=u'сообщение, которое будет сыграно: приблизительное время ожидания', related_name='queue_holdtime')
    queue_minutes = ForeignKey('asterfiles.SndFile', db_column='queue_minutes', blank=True, null=True, editable=True, db_index=True, help_text=u'сообщение, которое будет сыграно: минут', related_name='queue_minutes')
    queue_seconds = ForeignKey('asterfiles.SndFile', db_column='queue_seconds', blank=True, null=True, editable=True, db_index=True, help_text=u'сообщение, которое будет сыграно: секунд', related_name='queue_seconds')
    queue_lessthan = ForeignKey('asterfiles.SndFile', db_column='queue_lessthan', blank=True, null=True, editable=True, db_index=True, help_text=u'сообщение, которое будет сыграно: менее', related_name='queue_lessthan')
    queue_thankyou =  ForeignKey('asterfiles.SndFile', db_column='queue_thankyou', blank=True, null=True, editable=True, db_index=True, help_text=u'сообщение, которое будет сыграно: спасибо за ожидание', related_name='queue_thankyou')
    queue_reporthold = ForeignKey('asterfiles.SndFile', db_column='queue_reporthold', blank=True, null=True, editable=True, db_index=True, help_text=u'сообщение, которое будет сыграно: время ожидания', related_name='queue_reporthold')
    announce_frequency = PositiveIntegerField(blank=True, null=True, editable=True, db_index=True, verbose_name=u'Частота анонса')
    announce_round_seconds = PositiveIntegerField(blank=True, null=True, editable=True, db_index=True, verbose_name=u'Округление', help_text=u'Округлять минуты/секунды до этого значения')
    announce_holdtime = CharField(max_length=5, blank=True, null=True, editable=True, db_index=True, choices=YESNOONCE, verbose_name=u'Анонс предпологаемого время ожидания')
    retry = PositiveIntegerField(blank=True, null=True, editable=True, db_index=True, verbose_name=u'Повтор', help_text=u'Сколько мы можем ждать прежде чем попробывать звонить участникам очереди снова')
    wrapuptime = PositiveIntegerField(blank=True, null=True, editable=True, db_index=True, verbose_name=u'время завершения', help_text='сделать паузу во столько секунд прежде чем снова передавать вызов этому участнику')
    maxlen = PositiveIntegerField(blank=True, null=True, editable=True, db_index=True, verbose_name=u'максимальный размер очереди')
    servicelevel = PositiveIntegerField(blank=True, null=True, editable=True, db_index=True, default=0, verbose_name=u'Уровень обслуживания', help_text=u'опция используется для статистики об уровне обслуживания. Вы устанавливаете период времени, в котором звонки должен быть дан ответ. По умолчанию он установлен в 0 (отключен).')
    strategy = CharField(max_length=128, blank=True, null=True, editable=True, db_index=True, default='ringall', choices=STRATEGY, verbose_name=u"стратегия")
    joinempty = CharField(max_length=128, blank=True, null=True, editable=True, db_index=True)
    leavewhenempty = CharField(max_length=128, blank=True, null=True, editable=True, db_index=True)
    eventmemberstatus = BooleanField(default=False, null=False, blank=False, verbose_name=u'генерировать события статуса', choices=TRUEFALSE, db_index=True)
    eventwhencalled = BooleanField(default=False, null=False, blank=False, verbose_name=u'генерировать события вызовов', choices=TRUEFALSE, db_index=True)
    reportholdtime = BooleanField(default=False, null=False, blank=False, verbose_name=u'сообщать сколько ждал абонент', help_text=u'Эта опция очень полезна. Когда установлено Да, член очереди, который отвечает, услышит, как долго абонент был в ожидании и слушал музыку ожидания.', choices=TRUEFALSE, db_index=True)
    memberdelay = PositiveIntegerField(blank=True, null=True, editable=True, db_index=True, help_text=u'Пауза в секундах, прежде чем агент будет соединен с абонентом из очереди', verbose_name=u'Пауза соединения', default=0)
    weight = PositiveIntegerField(default=1, blank=True, null=True, editable=True, db_index=True, verbose_name=u'вес очереди', help_text=u'Очереди с большим весом имеют больший приоритет в канале')
    timeoutrestart = BooleanField(null=False, blank=False, verbose_name='рестарт по таймауту', choices=TRUEFALSE, db_index=True, default=False, help_text=u'Если этот параметр имеет значение Да, и на входящей линии сигнал ЗАНЯТО или ПЕРЕГРУЗКА, агенты будут сброшенны по таймауту. Это может быть полезно с агентами, которые имеет разрешения для отмены вызова.')

    class Meta:
        unique_together = (('name', ),)
        verbose_name = u'Очередь'
        verbose_name_plural = u'Очереди'

    def __unicode__(self):
        return u'%s' % (self.name)

class QueueMember(Model):
    queue = ForeignKey("Queue", blank=False, null=False, verbose_name='очередь', db_index=True, db_column='queue_name')
    interface = CharField(max_length=128, blank=True, null=True, editable=True, db_index=True)
    penalty = PositiveIntegerField(blank=True, null=True, editable=True, db_index=True, verbose_name=u'пеннальти', help_text=u'Это какой-то приоритет. Идея в том, что система будет пытаться звонить первым агентам с более низким приоритетом, а агентов с высшим пеннальти будет пытаться вызвать после')

    class Meta:
        verbose_name = u'Участник очереди'
        verbose_name_plural = u'Участники очередей'

    def __unicode__(self):
        return u'%s' % (self.interface)

class QueueLog(Model):
    time = CharField(max_length=20, blank=False, null=True, editable=False, db_index=True)
    callid = CharField(max_length=32, blank=True, null=False, editable=False, db_index=True)
    queue = CharField(max_length=32, blank=True, null=False, editable=False, db_index=True, db_column='queuename')
    agent = CharField(max_length=32, blank=True, null=False, editable=False, db_index=True)
    event = CharField(max_length=32, blank=True, null=False, editable=False, db_index=True)
    data  = CharField(max_length=20, blank=True, null=False, editable=False)
    data1 = CharField(max_length=20, blank=True, null=False, editable=False)
    data2 = CharField(max_length=20, blank=True, null=False, editable=False)
    data3 = CharField(max_length=20, blank=True, null=False, editable=False)
    data4 = CharField(max_length=20, blank=True, null=False, editable=False)
    data5 = CharField(max_length=20, blank=True, null=False, editable=False)

    class Meta:
        verbose_name = u'Статистика очередей'

    def __unicode__(self):
        time = datetime.fromtimestamp(float(self.time)).strftime('%Y-%m-%d %H:%M:%S')
        call = Cdr.objects.get(uniqueid=self.callid)
        return u'%s' % (call)

class Conference(Model):
    COMMENTED=(
       (1,'Нет'),
       (0,'Да')
    )
    commented = SmallIntegerField(default=0, null=False, blank=False, verbose_name=u'включен', choices=COMMENTED)
    confno = CharField(max_length=80, blank=False, null=False, verbose_name=u'номер комнаты', db_index=True, primary_key=True)
    starttime = DateTimeField(editable=True, blank=False, null=False, verbose_name=u'начало открытия комнаты', db_index=True)
    endtime = DateTimeField(editable=True, blank=False, null=False, verbose_name=u'конец открытия комнаты', db_index=True)
    pin = CharField(max_length=20, blank=True, null=True, default=None, verbose_name=u'пин код', db_index=True)
    opts = CharField(max_length=100, blank=True, null=True, default=None, verbose_name=u'опции', db_index=True)
    adminpin = CharField(max_length=20, blank=True, null=True, default=None, verbose_name=u'пин код администратора', db_index=True)
    adminopts = CharField(max_length=100, blank=True, null=True, default=None, verbose_name=u'опции администратора', db_index=True)
    members = IntegerField(blank=True, null=True, default=0, verbose_name=u'число пользователей',)
    maxusers = IntegerField(blank=True, null=True, default=0, verbose_name=u'максимальное число пользователей',)

    def __unicode__(self):
        return u'Комната: %s начало действия: %s' % (self.confno, self.starttime)

    class Meta:
        unique_together = (('confno', 'starttime',),)
        verbose_name = u'комната конференций'
        verbose_name_plural = u'комнаты конференций'

